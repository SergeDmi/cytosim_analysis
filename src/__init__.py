"""
 Python package `cytolysis` provides a framework to represent and analyze the various results of a cytosim simulation
.. include:: ./README.md
"""
