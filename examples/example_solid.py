####### PACKAGES
from cytolysis import cytosim_analysis as ana
import numpy as np
# Filenames
import pkg_resources as pk
solids_file = pk.resource_filename( "cytolysis" , 'example_data/solids.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# Here we we just need to tell Object_set how to create solids from the report file
import sio_tools as sio
from cytolysis.objects import Object
def build(solid_set, reports=None ):
    [rows, a, b] = sio.getdata_lines(reports['solid'])
    for row in rows:
        solid_set.append(Object(position=row[2:5]))


def intercore_distance(frame):
    return np.linalg.norm(frame.objects["solid"]["core"][0].position
                          - frame.objects["solid"]["core"][1].position)

# Reading from report and config files
solid_reports = {"position": solids_file}

# Creating simulation representation
simul = ana.Simulation(solid_report={"core" : solid_reports },
                       config=config_file)
# Reading properties in object simul
print(" Simulation properties : %s" %simul.properties)
print(" Core properties : %s" % simul[0].objects["solid"]["core"].properties)
print(" Number of frames : %s" %len(simul))

# Declaration of analysis functions
analyzer = {'frame' : {'distance': intercore_distance } }

# Performing analysis
simul.make_analysis(analyzer=analyzer)
ana.export_analysis(simul.frames_analysis, 'frames_solid.csv')
