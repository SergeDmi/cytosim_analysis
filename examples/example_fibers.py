from cytolysis import cytosim_analysis as ana

# Filenames
import pkg_resources as pk
fibers_file = pk.resource_filename( "cytolysis" , 'example_data/fiber_points.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# Simple example functions for analysis
def count_points_in_fiber(fiber):
    shape = fiber.points.shape
    return shape[0]

def count_points_in_frame(frame):
    return sum(frame.analysis['microtubule']['pts_number'])

# Reading from report and config files
microtubule_reports={"points" : fibers_file}

# Creating simulation representation
simul = ana.Simulation(fibers_report={"microtubule": microtubule_reports},
                       config=config_file)

# Reading properties in object simul
print(" Simulation properties : %s" %simul.properties)
print(" Microtubule properties : %s" % simul[0].objects["fiber"]['microtubule'].properties )
print(" Number of frames : %s" %len(simul))

# Declaration of analysis functions
analyzer={'microtubule' : {'pts_number': count_points_in_fiber},
          'frame' : { 'pts_total': count_points_in_frame} }

# Performing analysis
simul.make_analysis(analyzer=analyzer)
# Saving to file
ana.export_analysis(simul[2].analysis['microtubule'], 'MT_pts_frame_2.csv')
ana.export_analysis(simul.frames_analysis, 'frames_total_MT_points.csv')

