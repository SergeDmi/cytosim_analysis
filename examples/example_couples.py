from cytolysis import cytosim_analysis as ana

# Filenames
import pkg_resources as pk
couples_file = pk.resource_filename( "cytolysis" , 'example_data/couples_states.txt')
config_file = pk.resource_filename( "cytolysis" , 'example_data/example.cym')

# A function to count the number of active couples in a frame
def count_active_couples(frame):
    count=0
    for couple in frame.objects["couple"]["complex"]:
        if couple.state==2:
            count += 1
    return count

# A function to average the number of active couples over all frames
def mean_count(simul):
    return simul.frames_analysis['active_couples'].mean()

# Reading from report and config files
crosslinker_reports = {"state": couples_file}

# Creating simulation representation
simul = ana.Simulation(couples_report={"complex": crosslinker_reports},
                       config=config_file)
# Reading properties in object simul
print(" Simulation properties : %s" %simul.properties)
print(" Crosslinker properties : %s" % simul[0].objects["couple"]['complex'].properties)
print(" Hand1 properties : %s" % simul[0].objects["couple"]['complex'].hand1_props)

# Declaration of analysis functions
analyzer = {'frame' : {'active_couples': count_active_couples },
            'simulation': {'mean_active_couples' : mean_count } }

# Performing analysis
simul.make_analysis(analyzer=analyzer)
# Saving to file
ana.export_analysis(simul.frames_analysis, 'frames_complex.csv')
ana.export_analysis(simul.analysis, 'simul_complex.csv')
