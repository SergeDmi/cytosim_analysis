Module cytolysis.fibers
=======================

Functions
---------

    
`get_segments(points)`
:   get_segment(points)
    computes segment vectors from a numpy array
    Assume point coordinates to be row vectors.
    points is thus a column of points

Classes
-------

`Fiber(*args, name='fiber', id=1, points=None, curvatures=array([None], dtype=object), position=None, direction=None, length=None, end2end=None, **kwargs)`
:   Fiber
    A class that contains a filament. Yep, a whole class for that.
     initiated by providing a numpy array points :
     points is a column of row vectors.
     each row vector is of the format x y z C (3D) or x y C (2D)
     with C the curvature

`Fiber_set(*args, id=1, config=None, name='fiber', read=None, build=None, type='fiber', **kwargs)`
:   Fiber_set
    A class that contains a list of filaments plus extra methods and properties
    Built upon the Object_set class, itself a class derived from List

    ### Ancestors (in MRO)

    * cytolysis.objects.Object_set
    * builtins.list

    ### Methods

    `build_fibers(self, *args, repoints=None, **kwargs)`
    :   A specific function to build the fibers from the reports

    `compute_deformation_energy(self, fiber, *args, **kwargs)`
    :

    `plot_fiber(self, fiber, *args, **kwargs)`
    :