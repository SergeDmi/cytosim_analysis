Module cytolysis.objects
========================

Classes
-------

`Object(*args, id=1, position=None, **kwargs)`
:   Object
    Generic object, not completely useless
    Most objects need id and position

`Object_set(*args, id=1, config=None, name=None, read=None, type=None, dim=3, **kwargs)`
:   Object_set
    A class that contains a list of objects plus extra methods and properties

    ### Ancestors (in MRO)

    * builtins.list

    ### Descendants

    * cytolysis.couples.Couple_set
    * cytolysis.fibers.Fiber_set

    ### Methods

    `analyze(self, obj, analyzer=None, *args, **kwargs)`
    :   Generic analyzer
            wraps user specified analysis

    `build_objects(self, *args, repoints=None, name=None, **kwargs)`
    :   Build from the numpy arrays
        Specific to an object type

    `plot(self, *args, sorter=None, plotter=None, **kwargs)`
    :   Plots from Object set, with a possibilty to sort which object to sort
        Saved ploted items to Object_set.plots

    `plot_objs(self, objs, *args, **kwargs)`
    :   Plot objs is called by Object_set's plot.
         A method to plot a list of objects

    `read_objects(self, *args, reports=None, build=None, **kwargs)`
    :   Reads from config file into a dictionary repoints of numpy arrays
        We expect somthing like :
        repoints["position"] = [[...]]
        repoints["force"] = [[...]]